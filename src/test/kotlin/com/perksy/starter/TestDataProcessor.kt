package com.perksy.starter

import io.vertx.core.CompositeFuture
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.concurrent.TimeUnit

@ExtendWith(VertxExtension::class)
class TestDataProcessor {
  @BeforeEach
  fun deploy_verticle(vertx: Vertx, testContext: VertxTestContext) {
    val streamer = vertx.deployVerticle(DataStreamer())
    val processor = vertx.deployVerticle(DataProcessor())

    CompositeFuture.all(streamer, processor).onComplete() {
      if(it.succeeded())
        testContext.completeNow()
      else
        testContext.failNow(it.cause())
    }
  }

  @Test
  fun verticle_deployed(vertx: Vertx, testContext: VertxTestContext) {
    testContext.completeNow()
  }

  @Test
  fun getsData(vertx: Vertx, testContext: VertxTestContext) {
    val client = WebClient.create(vertx)
    val requestData = json {
      obj(
        "language" to "Bislama"
      )
    }

    vertx.eventBus().send("data.user.filter", requestData)

    vertx.setTimer(5000) {

      client.get(8888, "localhost", "/realtime")
        .`as`(BodyCodec.jsonObject())
        .send()
        .onSuccess {
          val data = it.body()
          testContext.verify {
            assert(it.statusCode() == 200)
            assert(data.containsKey("count"))
            assert(data.containsKey("results"))
          }.completeNow()
        }
    }

    assert(testContext.awaitCompletion(10, TimeUnit.SECONDS))
    if(testContext.failed()) {
      throw testContext.causeOfFailure()
    }
  }
}
