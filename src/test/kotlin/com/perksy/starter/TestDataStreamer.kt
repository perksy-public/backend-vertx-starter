package com.perksy.starter

import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*
import java.util.concurrent.TimeUnit

@ExtendWith(VertxExtension::class)
class TestDataStreamer {

  @BeforeEach
  fun deploy_verticle(vertx: Vertx, testContext: VertxTestContext) {
    vertx.deployVerticle(DataStreamer(), testContext.succeeding<String> { _ -> testContext.completeNow() })
  }

  @Test
  fun verticle_deployed(vertx: Vertx, testContext: VertxTestContext) {
    testContext.completeNow()
  }

  @Test
  fun sendsEvents(vertx: Vertx, testContext: VertxTestContext) {
    var count = 2
    runBlocking {
      val uuid = UUID.randomUUID()

      vertx.eventBus().consumer<JsonObject>("data.user.request.${uuid}").handler {
        testContext.verify {
          assert(it.body().containsKey("first_name"))
        }

        if(it.headers().get("index").toInt() == 1) {
          testContext.completeNow()
        }
      }

      val msg = vertx.eventBus().request<JsonObject>(
        "data.user",
        count,
        DeliveryOptions().addHeader("responseAddress", "data.user.request.${uuid}")
      ).await()

      val body = msg.body()
      testContext.verify {
        assert(body.getBoolean("processing"))
      }
    }

    assert(testContext.awaitCompletion(10, TimeUnit.SECONDS))
    if(testContext.failed()) {
      throw testContext.causeOfFailure()
    }
  }
}
