package com.perksy.starter

import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.eventbus.Message
import io.vertx.core.impl.logging.LoggerFactory
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.awaitEvent
import io.vertx.kotlin.coroutines.receiveChannelHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.streams.asSequence

/**
 * Sends data to whoever requests at the user.data address
 * Expects an Int as a message to that request and a returnAddress in the DeliveryOptions
 *
 * Packets responded with are json objects with the following schema:
 * {
 *   id: Int,
 *   first_name: String,
 *   last_name: String,
 *   email: String,
 *   gender: String,
 *   language: String
 * }
 *
 */
class DataStreamer: CoroutineVerticle() {
  var eventJob: Job? = null

  override suspend fun start() {
    // Definitely better ways to do this
    val data = this::class.java.classLoader.getResource("MOCK_DATA.json").readText()
    val jsonObj = JsonArray(data)

    // Start sending events
    eventJob = launch {
      sendEvents(jsonObj)
    }
    LOGGER.debug("Data Streamer started")
  }

  override suspend fun stop() {
    super.stop()
    // Just an example. The parent coroutine scope is managed by vertx in the CoroutineVertcle
    eventJob?.cancel()
  }

  /**
   * Sends the events in a suspendable coroutine function so as to not block the event loop
   */
  suspend fun sendEvents(jsonObj: JsonArray) {
    // Converts the json array into a stream that we can sequentially request users from
    val userStream = jsonObj.stream().asSequence().map { e -> e as JsonObject }.iterator()

    // Allows the use of Kotlin's channel synchronous-looking structure. This isn't necessary
    val adapter = vertx.receiveChannelHandler<Message<Int>>()
    // Set up a consumer at the "data.user" address for us to recieve requests for user data
    vertx.eventBus().consumer<Int>("data.user").handler(adapter)

    // Keep going forever in the coroutine worker
    while (true) {
      // Waits until a message is received by the channel
      val message = adapter.receive()
      // How many users to send back
      val count = message.body()

      // Replies to indicate that we got the message
      // This json { obj(...) } wrapper generates JsonObject types that vertx understands
      message.reply(json {
        obj(
          "processing" to true
        )
      })

      // We'll do this as many times as was requested
      repeat(count) { index ->
        // Arbitrary wait
        awaitEvent<Long> { vertx.setTimer(1000, it) }

        // Get the next user in the stream
        val user = userStream.next()
        LOGGER.info(user)

        // Get the return address from the header so we can reply with the data
        val returnAddress = message.headers().get("responseAddress")

        // Send back the data
        vertx.eventBus().send(returnAddress, user, DeliveryOptions().addHeader("index", index.toString()))
      }
    }
  }

  companion object {
    // Grabs our default logger
    val LOGGER = LoggerFactory.getLogger(this::class.java)
  }
}
