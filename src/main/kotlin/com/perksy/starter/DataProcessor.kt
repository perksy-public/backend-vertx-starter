package com.perksy.starter

import io.vertx.kotlin.coroutines.CoroutineVerticle

/**
 * Processes data requested from the data.user.filter address
 *
 * Has a consumer channel that takes a request for some kind of data
 * It then starts requesting from our data stream and starts filtering on the type of data requested and keeps a
 * running tally that can be retrieved at the /realtime endpoint
 *
 * Expects a JsonObject as a configuration for what to filter on which is just a partial of the user data's schema:
 * User {
 *   id?: Int,
 *   first_name?: String,
 *   last_name?: String,
 *   email?: String,
 *   gender?: String,
 *   language?: String
 * }
 *
 * When the endpoint is requested it sends json with
 *
 * {
 *   count: Int,
 *   results: User[]
 * }
 *
 * These packets could be received in a variable amount of time
 */
class DataProcessor: CoroutineVerticle() {
  override suspend fun start() {

  }
}
